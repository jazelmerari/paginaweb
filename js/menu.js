import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getDatabase, ref, set, onChildAdded, onChildRemoved, remove, update, onChildChanged } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js';
import { getStorage, ref as storageRef, uploadBytesResumable, getDownloadURL } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js';

// Configuración de Firebase
const firebaseConfig = {
    apiKey: "AIzaSyCMewBdiMlpfN52u6qYQgFDCRGAfpsdhzM",
    authDomain: "proyectofmj.firebaseapp.com",
    databaseURL: "https://proyectofmj-default-rtdb.firebaseio.com",
    projectId: "proyectofmj",
    storageBucket: "proyectofmj.appspot.com",
    messagingSenderId: "570499264757",
    appId: "1:570499264757:web:9fd2e15a7c345d82d595bc"
};

const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage(app);

function uploadImage(file, callback) {
    const sRef = storageRef(storage, 'imagen/' + Date.now() + '-' + file.name);
    const uploadTask = uploadBytesResumable(sRef, file);


    uploadTask.on('state_changed',
        (snapshot) => {
            const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log('Upload is ' + progress + '% done');
        },
        (error) => {
            console.error('Error uploading image:', error);

        },
        () => {
            getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
                callback(downloadURL);

            });
        }
    );
}
function resetForm() {
    document.getElementById('nombre').value = '';
    document.getElementById('precio').value = '';
    document.getElementById('descripcion').value = '';
    document.getElementById('cantidad').value = '';
    document.getElementById('productImage').value = '';
}

document.getElementById('productForm').addEventListener('submit', function (event) {
    event.preventDefault();

    const nombre = document.getElementById('nombre').value;
    const precio = document.getElementById('precio').value;
    const descripcion = document.getElementById('descripcion').value;
    const cantidad = document.getElementById('cantidad').value;
    const imageFile = document.getElementById('productImage').files[0];
    const existingKey = document.getElementById('productId').value;

    saveProduct(nombre, precio, descripcion, cantidad, imageFile, existingKey);

    resetForm();  // Aquí limpiamos el formulario
});

function saveProduct(nombre, precio, descripcion, cantidad, imageFile, existingKey) {
    if (!nombre || !precio || !descripcion || !cantidad) {
        console.error("Datos incompletos, no se guardará el producto");
        return;
    }
    function storeData(imageUrl) {
        const data = {
            nombre: nombre,
            precio: precio,
            descripcion: descripcion,
            cantidad: cantidad,
            imageUrl: imageUrl
        };
        if (existingKey) {
            const productRef = ref(db, `productos/` + existingKey);
            update(productRef, data);
        } else {
            const newProductRef = ref(db, `productos/` + Date.now());
            set(newProductRef, data);
        }
    }

    if (imageFile) {
        uploadImage(imageFile, (imageUrl) => {
            storeData(imageUrl);
        });
    } else {
        storeData(null);
    }
}

document.getElementById('productForm').addEventListener('submit', function (event) {
    event.preventDefault();

    const nombre = document.getElementById('nombre').value;
    const precio = document.getElementById('precio').value;
    const descripcion = document.getElementById('descripcion').value;
    const cantidad = document.getElementById('cantidad').value;
    const imageFile = document.getElementById('productImage').files[0];
    const existingKey = document.getElementById('productId').value;

    saveProduct(nombre, precio, descripcion, cantidad, imageFile, existingKey);

    // Limpiar el formulario después de guardar el producto
    cancelEditar();
});

function deleteProduct(productId) {
    const productRef = ref(db, `productos/` + productId);
    remove(productRef);
}

function updateProductInView(productId, data) {
    // Actualizar los elementos de la interfaz de usuario con el productId y los nuevos datos
}

function updateProductInTable(productId, data) {
    // Actualizar la fila en la tabla con el productId y los nuevos datos
}

onChildAdded(ref(db, 'productos/'), (data) => {
    const newProduct = data.val();
    console.log(`Producto nuevo añadido: ${newProduct.nombre}`);
    // Agregar el producto nuevo a la vista
});

onChildRemoved(ref(db, 'productos/'), (data) => {
    const deletedProduct = data.val();
    console.log(`Producto eliminado: ${deletedProduct.nombre}`);
    // Remover el producto de la vista
});

onChildChanged(ref(db, 'productos/'), (data) => {
    const updatedProduct = data.val();
    console.log(`Producto actualizado: ${updatedProduct.nombre}`);
    updateProductInView(data.key, updatedProduct);
    // Actualizar el producto en la vista
});
function insertProductInTable(key, product) {
    const tableBody = document.getElementById('productTable').getElementsByTagName('tbody')[0];
    const newRow = tableBody.insertRow();

    // Añadir datos a la fila
    newRow.innerHTML = `
      <td>${product.nombre}</td>
      <td>${product.precio}</td>
      <td>${product.descripcion}</td>
      <td>${product.cantidad}</td>
      <td><img src="${product.imageUrl || ''}" alt="${product.nombre}" height="50"/></td>
      <td>
        <button onclick="fillFormForEdit('${key}')">Editar</button>
        <button onclick="deleteProduct('${key}')">Eliminar</button>
      </td>
    `;
    newRow.id = `product-${key}`;
}

function fillFormForEdit(key) {
    const productRef = ref(db, `productos/` + key);
    onValue(productRef, (snapshot) => {
        const product = snapshot.val();
        document.getElementById('nombre').value = product.nombre;
        document.getElementById('precio').value = product.precio;
        document.getElementById('descripcion').value = product.descripcion;
        document.getElementById('cantidad').value = product.cantidad;
        document.getElementById('productId').value = key;  // Guarda la clave del producto en el campo oculto
        // No puedes establecer el archivo de imagen en el input debido a políticas de seguridad del navegador
    });
}

onChildAdded(ref(db, 'productos/'), (snapshot) => {
    const key = snapshot.key;
    const product = snapshot.val();
    insertProductInTable(key, product);
});

onChildChanged(ref(db, 'productos/'), (snapshot) => {
    const key = snapshot.key;
    const product = snapshot.val();
    const existingRow = document.getElementById(`product-${key}`);
    if (existingRow) {
        existingRow.innerHTML = `
        <td>${product.nombre}</td>
        <td>${product.precio}</td>
        <td>${product.descripcion}</td>
        <td>${product.cantidad}</td>
        <td><img src="${product.imageUrl || ''}" alt="${product.nombre}" height="50"/></td>
        <td>
          <button onclick="fillFormForEdit('${key}')">Editar</button>
          <button onclick="deleteProduct('${key}')">Eliminar</button>
        </td>
      `;
    }
});

onChildRemoved(ref(db, 'productos/'), (snapshot) => {
    const key = snapshot.key;
    const rowToRemove = document.getElementById(`product-${key}`);
    if (rowToRemove) {
        rowToRemove.remove();
    }
});

// La función deleteProduct ya se proporcionó previamente.
